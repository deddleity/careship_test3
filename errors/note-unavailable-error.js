/**
 * Created by diana on 1/04/18.
 */
class NoteUnavailableException extends require('../errors/AppError') {
    constructor (message) {
        super(message || 'Note unavailable exception', 400);
    }
}
module.exports = NoteUnavailableException;