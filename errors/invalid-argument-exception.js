/**
 * Created by diana on 1/04/18.
 */
class InvalidArgumentException extends require('../errors/AppError') {
    constructor (message) {
        super(message || 'Invalid argument Exception', 400);
    }
};
module.exports = InvalidArgumentException;