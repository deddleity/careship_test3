const expect  = require('chai').expect;
let Calculator = require('../services/calculator-service');
let Validate = require('../validations/cash-validation');
const invalid = require('../errors/invalid-argument-exception');
const noteUnavailable = require('../errors/note-unavailable-error');

describe('CalculatorMachine ', () => {
    it('return one per each note', done => {
        let cash = new Calculator(180);

        expect(() => cash.getPerEachNotes().to.equal([100,50,20,10]));
        done();
    });

    it('returns one per each note and 2 of 100', done => {
        let cash = new Calculator(180);

        expect(() => cash.getPerEachNotes().to.equal([100,100,50,20,10]));
        done();
    });
});


describe('Validations ', () => {
    it('negative number should throw error', done => {
        let validate = new Validate(-180);

        expect(() => validate.validateAmountInput().to.throw(invalid));
        done();
    });

    it('if is not divisible by 10 should throw error', done => {
        let validate = new Validate(185);

        expect(() => validate.validateAmountInput().to.throw(noteUnavailable));
        done();
    });
});