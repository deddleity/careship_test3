/**
 * Created by diana on 30/03/18.
 */
const express = require('express');
const router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
    res.render('index', { title: 'Careship test access port 3030 for the front 3070 /api/1/cash for restAPI' });
});

module.exports = router;