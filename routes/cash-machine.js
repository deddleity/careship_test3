/**
 * Created by diana on 30/03/18.
 */
const express = require('express');
const cashRouter = express.Router();
const calculator = require('../services/calculator-service');
const validations = require('../validations/cash-validation');

/* Post cash machine. */
cashRouter.post('/cash', function(req, res, next) {
    let requiredValue = req.body;

    let validate = new validations(requiredValue.amount);
    validate.validateAmountInput();

    let calculate = new calculator(requiredValue.amount);
    let responseJson = calculate.getPerEachNotes();

    res.json(responseJson);
});

module.exports = cashRouter;