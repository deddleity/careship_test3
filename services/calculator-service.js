/**
 * Created by diana on 30/03/18.
 */
class Calculator {
    constructor(requiredAmount) {
        this.avialiablenotes = [100, 50, 20, 10];
        this.requiredAmount = requiredAmount;
        this.resultNotes = [];
        this.notes = [];
    }

    getTotalNotes() {
        this.leftAmount = this.requiredAmount;

        this.resultNotes = this.avialiablenotes.map(function(note, index, array) {
            let howMany = {};

            howMany[note] = Math.floor(this.leftAmount / note);

            this.leftAmount %= note;

            return howMany
        }, this);
    }

    getPerEachNotes() {
        this.getTotalNotes();
        let notes = this.resultNotes;
        let perNote = [];
        let counter = 0;
        for (let value of notes) {
            let note = this.avialiablenotes[counter];
            if(value !== 0){
                let many = value[note];
                for (let i; many > 0; many--) {
                    perNote.push(note);
                }
            }
            counter++;
        }
        return perNote;

    }
}

module.exports = Calculator;