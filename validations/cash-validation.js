/**
 * Created by diana on 31/03/18.
 */
const invalid = require('../errors/invalid-argument-exception');
const noteUnavailable = require('../errors/note-unavailable-error');

class cashValidations
{
    constructor(amount) {
        this.amount = amount
    }

    validateAmountInput() {
        let amount = this.amount;

        if (isNaN(amount)) {
            throw new invalid();
        }

        if (!Number.isInteger(amount / 10)){
            throw new noteUnavailable();
        }

        if (!(amount >= 0)) {
            throw new invalid();
        }
    }

}
module.exports = cashValidations;